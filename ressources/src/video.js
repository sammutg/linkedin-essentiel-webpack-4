import 'bootstrap';
import './styles/styles.scss';
import videoJs from 'video.js';
import tram from './medias/tram.mp4';

videoJs(document.querySelector('#tramVideo'), {
    control: true
}).ready(function () {
    let vPlayer = this;
    vPlayer.src(tram)
});