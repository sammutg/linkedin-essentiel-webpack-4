const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');

module.exports = {
    entry: {
        'index': './src/index.js',
        'video': './src/video.js'
    },
    output: {
        filename: 'js/[name]-[contenthash].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [{
                test: /\.(jpg|jpeg|gif|png)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 51200,
                        name: '[name]-[contenthash].[ext]',
                        outputPath: 'images'
                    }
                }]
            },
            {
                test: /\.mp4$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name]-[contenthash].[ext]',
                        outputPath: 'medias'
                    }
                }]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name]-[contenthash].[ext]',
                        outputPath: 'fonts'
                    }
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new htmlWebpackPlugin({
            template: 'src/templates/index.html',
            filename: 'index.html',
            title: 'Essentiel Webpack 4 - Gérer plusieurs pages avec Webpack',
            chunks: ['index']
        }),
        new htmlWebpackPlugin({
            template: 'src/templates/video.html',
            filename: 'video.html',
            title: 'Essentiel Webpack 4 - Video',
            chunks: ['video']
        })
    ]
};