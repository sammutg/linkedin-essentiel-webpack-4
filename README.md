https://www.linkedin.com/learning/l-essentiel-de-webpack-4/installer-webpack

Dans cette formation destinée aux développeurs web, Damien Bruyndonckx vous propose de découvrir Webpack 4, un incontournable bundle open source de modules JavaScript. Après avoir passé en revue les prérequis et procédé à l'installation des outils, vous configurerez Webpack puis vous verrez comment travailler avec les loaders et différents plug-ins. Vous apprendrez également à optimiser le code et à gérer les pages multiples. À la fin de cette formation, vous maîtriserez la compatibilité, les dépendances et la légèreté de vos applications grâce à Webpack.

1 h 51 min Débutant + Intermédiaire Publié le 19/7/2019

